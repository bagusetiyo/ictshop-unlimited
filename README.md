# Changelog ICTShop Unlimited

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

# [5.1.0.6.14] - 2018-06-06
## Menu Laporan Penjualan
### Changed :
- Tambah informasi diskon faktur.
- Pencarian data menggunakan tombol cari.

## Menu Laporan Laba (Rugi)
### Fixed :
- Fix informasi periode tidak muncul di judul laporan.